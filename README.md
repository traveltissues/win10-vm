# things i learned trying to kvm win10

* To get audio to work at all, edit /etc/libvirt/qemu.conf by appending the following then restart the service:

```
user = <username>
nographics_allow_host_audio = 1
```

* run `ansible-playbook -K setup.yml` and browse the vm using virtualmachinemanager

* when performing a custom install of win10 (install win 10 pro for workstations - home only supports one socket prior to activattion) you'll need to load some drivers to discover storage:

(taken from https://wiki.unraid.net/index.php/UnRAID_Manual_6#Loading_the_VirtIO_Drivers_During_Installation)

    Loading the VirtIO Drivers During Installation

       * During the Windows installation process, you will reach a point where "no disks are found", this is expected behavior.
       * Click Browse on this screen, then navigate to the virtio-win CD-ROM.
       * You will need to load the following drivers in the following order:
           * Balloon
           * NetKVM
           * vioserial
           * viostor (be sure to load this one last)
       * For each driver that needs to be loaded, you will navigate to the driver folder, then the OS version, then the amd64 subfolder (never click to load the x86 folder)
       * After each driver is loaded, you will need to click the Browse button again to load the next driver.
       * After loading the viostor driver, your virtual disk will then appear to select for installation and you can continue installing Windows as normal.
       * After Windows is completely installed, you can install the guest agent, which improves host to guest management
           * Open Windows File Explorer
           * Browse to the virtual CD-ROM for virtio-win again, and then open the guest-agent folder
           * Double-click to launch the qemu-ga-x64.msi installer (this process will be rather quick)
